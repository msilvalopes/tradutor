<?php
namespace App\Model\Table;

use App\Model\Entity\Palavra;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Palavras Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Idiomas
 */
class PalavrasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('palavras');
        $this->displayField('vocabulo');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Idiomas', [
            'foreignKey' => 'idioma_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('vocabulo', 'create')
            ->notEmpty('vocabulo');

        $validator
            ->requirePresence('significado', 'create')
            ->notEmpty('significado');

        $validator
            ->add('active', 'valid', ['rule' => 'numeric'])
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->add('validated', 'valid', ['rule' => 'numeric'])
            ->requirePresence('validated', 'create')
            ->notEmpty('validated');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['idioma_id'], 'Idiomas'));
        $rules->add($rules->existsIn(['classe_gramatical_id'], 'ClasseGramatical'));
        return $rules;
    }
}

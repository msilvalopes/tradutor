<?php
namespace App\Model\Table;

use App\Model\Entity\Idioma;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Idiomas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Idiomas
 * @property \Cake\ORM\Association\HasMany $Idiomas
 * @property \Cake\ORM\Association\HasMany $Palavras
 */
class IdiomasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('idiomas');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('IdiomasBase', [
            'foreignKey' => 'idioma_id',
            'joinType' => 'LEFT',
            'className' => 'Idiomas'
        ]);
        $this->hasMany('Idiomas', [
            'foreignKey' => 'idioma_id'
        ]);
        $this->hasMany('Palavras', [
            'foreignKey' => 'idioma_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['idioma_id'], 'Idiomas'));
        return $rules;
    }
}

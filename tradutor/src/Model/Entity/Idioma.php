<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Idioma Entity.
 *
 * @property int $id
 * @property string $nome
 * @property int $idioma_id
 * @property string $descricao
 * @property \Cake\I18n\Time $created
 * @property \App\Model\Entity\Idioma[] $idiomas
 * @property \App\Model\Entity\Palavra[] $palavras
 */
class Idioma extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}

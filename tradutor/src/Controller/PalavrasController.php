<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Palavras Controller
 *
 * @property \App\Model\Table\PalavrasTable $Palavras
 */
class PalavrasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Idiomas']
        ];
        $this->set('palavras', $this->paginate($this->Palavras));
        $this->set('_serialize', ['palavras']);
    }

    /**
     * View method
     *
     * @param string|null $id Palavra id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $palavra = $this->Palavras->get($id, [
            'contain' => ['Idiomas']
        ]);
        $this->set('palavra', $palavra);
        $this->set('_serialize', ['palavra']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $palavra = $this->Palavras->newEntity();
        if ($this->request->is('post')) {
            $palavra = $this->Palavras->patchEntity($palavra, $this->request->data);
            if ($this->Palavras->save($palavra)) {
                $this->Flash->success(__('The palavra has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The palavra could not be saved. Please, try again.'));
            }
        }
        $idiomas = $this->Palavras->Idiomas->find('list', ['limit' => 200]);
        $this->set(compact('palavra', 'idiomas'));
        $this->set('_serialize', ['palavra']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Palavra id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $palavra = $this->Palavras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $palavra = $this->Palavras->patchEntity($palavra, $this->request->data);
            if ($this->Palavras->save($palavra)) {
                $this->Flash->success(__('The palavra has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The palavra could not be saved. Please, try again.'));
            }
        }
        $idiomas = $this->Palavras->Idiomas->find('list', ['limit' => 200]);
        $this->set(compact('palavra', 'idiomas'));
        $this->set('_serialize', ['palavra']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Palavra id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $palavra = $this->Palavras->get($id);
        if ($this->Palavras->delete($palavra)) {
            $this->Flash->success(__('The palavra has been deleted.'));
        } else {
            $this->Flash->error(__('The palavra could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

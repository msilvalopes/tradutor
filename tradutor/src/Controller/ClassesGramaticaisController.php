<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\ClasseGramaticalTable;
use Cake\Event\Event;
/**
 * ClassesGramaticais Controller
 *
 * @property \App\Model\Table\ClassesGramaticaisTable $ClassesGramaticais
 */

class ClassesGramaticaisController extends AppController
{

    public $paginate = [
        'ClasseGramatical'
    ];
    public $models = [
        'ClasseGramatical'
    ];
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        $this->loadModel('ClasseGramatical');
        parent::beforeRender($event);

    }
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'ClasseGramatical' => [
                'contain' => ['Idiomas']
            ]
        ];
        $this->set('classesGramaticais', $this->paginate('ClasseGramatical'));
        $this->set('_serialize', ['classesGramaticais']);
    }

    /**
     * View method
     *
     * @param string|null $id Classe Gramatical id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $classesGramaticai = $this->ClasseGramatical->get($id, [
            'contain' => ['Idiomas']
        ]);
        $this->set('classesGramaticai', $classesGramaticai);
        $this->set('_serialize', ['classesGramaticai']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $classesGramaticai = $this->ClasseGramatical->newEntity();
        if ($this->request->is('post')) {
            $classesGramaticai = $this->ClasseGramatical->patchEntity($classesGramaticai, $this->request->data);
            if ($this->ClasseGramatical->save($classesGramaticai)) {
                $this->Flash->success(__('The classe gramatical has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The classe gramatical could not be saved. Please, try again.'));
            }
        }
        $idiomas = $this->ClasseGramatical->Idiomas->find('list', ['limit' => 200]);
        $this->set(compact('classesGramaticai', 'idiomas'));
        $this->set('_serialize', ['classesGramaticai']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Classe Gramatical id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $classesGramaticai = $this->ClasseGramatical->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $classesGramaticai = $this->ClasseGramatical->patchEntity($classesGramaticai, $this->request->data);
            if ($this->ClasseGramatical->save($classesGramaticai)) {
                $this->Flash->success(__('The classe gramatical has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The classe gramatical could not be saved. Please, try again.'));
            }
        }
        $idiomas = $this->ClasseGramatical->Idiomas->find('list', ['limit' => 200]);
        $this->set(compact('classesGramaticai', 'idiomas'));
        $this->set('_serialize', ['classesGramaticai']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Classe Gramatical id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classesGramaticai = $this->ClasseGramatical->get($id);
        if ($this->ClasseGramatical->delete($classesGramaticai)) {
            $this->Flash->success(__('The classe gramatical has been deleted.'));
        } else {
            $this->Flash->error(__('The classe gramatical could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Idioma'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Palavras'), ['controller' => 'Palavras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Palavra'), ['controller' => 'Palavras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="idiomas index large-9 medium-8 columns content">
    <h3><?= __('Idiomas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('idioma_id', _('Grupo de Idioma')) ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($idiomas as $idioma):  ?>
            <tr>
                <td><?= $this->Number->format($idioma->id) ?></td>
                <td><?= h($idioma->nome) ?></td>
                <td><?= empty($idioma->idiomas_base->nome)?'-':$idioma->idiomas_base->nome ?></td>
                <td><?= h($idioma->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $idioma->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $idioma->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $idioma->id], ['confirm' => __('Are you sure you want to delete # {0}?', $idioma->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

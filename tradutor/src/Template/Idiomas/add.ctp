<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Idiomas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Palavras'), ['controller' => 'Palavras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Palavra'), ['controller' => 'Palavras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="idiomas form large-9 medium-8 columns content">
    <?= $this->Form->create($idioma) ?>
    <fieldset>
        <legend><?= __('Add Idioma') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('idioma_id', [
                'options' => $idiomas_base,
                'label' => _('Grupo de Idioma'), 
                'empty' => _('Escolha um')
            ]);
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

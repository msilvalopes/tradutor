<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $idioma->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $idioma->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Idiomas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Palavras'), ['controller' => 'Palavras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Palavra'), ['controller' => 'Palavras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="idiomas form large-9 medium-8 columns content">
    <?= $this->Form->create($idioma) ?>
    <fieldset>
        <legend><?= __('Edit Idioma') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('idioma_id');
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

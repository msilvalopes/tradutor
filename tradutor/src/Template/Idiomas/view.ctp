<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Idioma'), ['action' => 'edit', $idioma->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Idioma'), ['action' => 'delete', $idioma->id], ['confirm' => __('Are you sure you want to delete # {0}?', $idioma->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Idiomas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Idioma'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Palavras'), ['controller' => 'Palavras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Palavra'), ['controller' => 'Palavras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="idiomas view large-9 medium-8 columns content">
    <h3><?= h($idioma->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($idioma->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($idioma->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Idioma Id') ?></th>
            <td><?= $this->Number->format($idioma->idioma_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($idioma->created) ?></tr>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($idioma->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Idiomas') ?></h4>
        <?php if (!empty($idioma->idiomas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Idioma Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($idioma->idiomas as $idiomas): ?>
            <tr>
                <td><?= h($idiomas->id) ?></td>
                <td><?= h($idiomas->nome) ?></td>
                <td><?= h($idiomas->idioma_id) ?></td>
                <td><?= h($idiomas->descricao) ?></td>
                <td><?= h($idiomas->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Idiomas', 'action' => 'view', $idiomas->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Idiomas', 'action' => 'edit', $idiomas->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Idiomas', 'action' => 'delete', $idiomas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $idiomas->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Palavras') ?></h4>
        <?php if (!empty($idioma->palavras)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Vocabulo') ?></th>
                <th><?= __('Idioma Id') ?></th>
                <th><?= __('Significado') ?></th>
                <th><?= __('Active') ?></th>
                <th><?= __('Validated') ?></th>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($idioma->palavras as $palavras): ?>
            <tr>
                <td><?= h($palavras->id) ?></td>
                <td><?= h($palavras->vocabulo) ?></td>
                <td><?= h($palavras->idioma_id) ?></td>
                <td><?= h($palavras->significado) ?></td>
                <td><?= h($palavras->active) ?></td>
                <td><?= h($palavras->validated) ?></td>
                <td><?= h($palavras->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Palavras', 'action' => 'view', $palavras->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Palavras', 'action' => 'edit', $palavras->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Palavras', 'action' => 'delete', $palavras->id], ['confirm' => __('Are you sure you want to delete # {0}?', $palavras->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>

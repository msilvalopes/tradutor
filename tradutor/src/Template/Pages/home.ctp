<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<div class="home-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?= $this->Html->image('/img/logo_dictionary.svg', ['width' => '300']) ?>
                <h1>Dicionário Tupi-Guarani &amp; Português</h1>
                <?php
                $myTemplates = [
                    'inputContainer' => '<div class="col-md-6">{{content}}</div>',
                ];
                $this->Form->templates($myTemplates);
                $this->assign('title',$title); 
        
                echo $this->Form->create();
                $langs = [1 => _('Automatico'), _('PT -> TG'), _('TG -> PT')];
                echo $this->Form->input('palavra',['label' => _('Palavra').': ']);
                echo $this->Form->input('modo', ['options' => $langs, 'label' => _('Modo'). ': ' ]);
                echo '<hr class="col-md-12">';
                echo $this->Form->button(_('Traduzir'));
                echo $this->Form->end();

                ?> 
        	</div>
        </div>
    </div>
</div>
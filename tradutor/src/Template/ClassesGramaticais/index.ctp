<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Classes Gramaticai'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classesGramaticais index large-9 medium-8 columns content">
    <h3><?= __('Classes Gramaticais') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome_classe') ?></th>
                <th><?= $this->Paginator->sort('abreviatura') ?></th>
                <th><?= $this->Paginator->sort('idioma_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classesGramaticais as $classesGramaticai): ?>
            <tr>
                <td><?= $this->Number->format($classesGramaticai->id) ?></td>
                <td><?= h($classesGramaticai->nome_classe) ?></td>
                <td><?= h($classesGramaticai->abreviatura) ?></td>
                <td><?= $classesGramaticai->has('idioma') ? $this->Html->link($classesGramaticai->idioma->nome, ['controller' => 'Idiomas', 'action' => 'view', $classesGramaticai->idioma->id]) : '' ?></td>
                <td><?= h($classesGramaticai->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $classesGramaticai->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $classesGramaticai->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $classesGramaticai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classesGramaticai->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

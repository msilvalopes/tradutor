<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $classesGramaticai->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $classesGramaticai->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Classes Gramaticais'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="classesGramaticais form large-9 medium-8 columns content">
    <?= $this->Form->create($classesGramaticai) ?>
    <fieldset>
        <legend><?= __('Edit Classes Gramaticai') ?></legend>
        <?php
            echo $this->Form->input('nome_classe');
            echo $this->Form->input('abreviatura');
            echo $this->Form->input('idioma_id', ['options' => $idiomas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

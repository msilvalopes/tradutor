<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Classes Gramaticai'), ['action' => 'edit', $classesGramaticai->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Classes Gramaticai'), ['action' => 'delete', $classesGramaticai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classesGramaticai->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Classes Gramaticais'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Classes Gramaticai'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="classesGramaticais view large-9 medium-8 columns content">
    <h3><?= h($classesGramaticai->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome Classe') ?></th>
            <td><?= h($classesGramaticai->nome_classe) ?></td>
        </tr>
        <tr>
            <th><?= __('Idioma') ?></th>
            <td><?= $classesGramaticai->has('idioma') ? $this->Html->link($classesGramaticai->idioma->nome, ['controller' => 'Idiomas', 'action' => 'view', $classesGramaticai->idioma->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($classesGramaticai->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($classesGramaticai->created) ?></tr>
        </tr>
    </table>
</div>

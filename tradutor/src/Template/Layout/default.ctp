<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dicionário:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->script('jquery-1.11.3.min.js');?> 
    <?= $this->Html->script('bootstrap.min.js');?> 
    <?= $this->Html->css('bootstrap.min.css');?> 
    <?= $this->Html->css('dictionary.css');?> 
    <?= $this->Html->meta('icon') ?> 
    <?= $this->fetch('meta') ?> 
    <?= $this->fetch('css') ?> 
    <?= $this->fetch('script') ?> 
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only"><?=_('Alternar navegação')?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="javascript:void(0)"><?= $this->fetch('title') ?></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/"><?=_('Home')?> <span class="sr-only">(atual)</span></a></li>
                    <li class=""><a href="/translate/origin/portugues"><?=_('A partir do português')?></a></li>
                    <li class=""><a href="/translate/origin/tupi-guarani"><?=_('A partir do tupi-guarani')?></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">
                            <?=_('Dicionários')?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/dictionary/tupi-guarani"><?=_('Tupi-guarani')?></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">
                            <?=_('Gestão')?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/idiomas"><?=_('Idiomas')?></a></li>
                            <li><a href="/palavras"><?=_('Palavras')?></a></li>
                            <li><a href="/classes_gramaticais"><?=_('Classes gramaticais')?></a></li>
                        </ul>
                    </li>
                    <li class=""><a href="/sobre"><?=_('Sobre')?></a></li>
                </ul>
            </div>
        </div><!-- /.container-fluid -->
    </nav>
    <?php #= #$this->Flash->render() ?>
    <?= $this->fetch('content') ?>
    <footer>
    </footer>
</body>
</html>

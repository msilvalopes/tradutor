<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Palavra'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="palavras index large-9 medium-8 columns content">
    <h3><?= __('Palavras') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('vocabulo') ?></th>
                <th><?= $this->Paginator->sort('idioma_id') ?></th>
                <th><?= $this->Paginator->sort('active') ?></th>
                <th><?= $this->Paginator->sort('validated') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($palavras as $palavra): ?>
            <tr>
                <td><?= $this->Number->format($palavra->id) ?></td>
                <td><?= h($palavra->vocabulo) ?></td>
                <td><?= $palavra->has('idioma') ? $this->Html->link($palavra->idioma->id, ['controller' => 'Idiomas', 'action' => 'view', $palavra->idioma->id]) : '' ?></td>
                <td><?= $this->Number->format($palavra->active) ?></td>
                <td><?= $this->Number->format($palavra->validated) ?></td>
                <td><?= h($palavra->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $palavra->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $palavra->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $palavra->id], ['confirm' => __('Are you sure you want to delete # {0}?', $palavra->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<pre><?=var_dump($palavras)?></pre>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Palavra'), ['action' => 'edit', $palavra->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Palavra'), ['action' => 'delete', $palavra->id], ['confirm' => __('Are you sure you want to delete # {0}?', $palavra->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Palavras'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Palavra'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="palavras view large-9 medium-8 columns content">
    <h3><?= h($palavra->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Vocabulo') ?></th>
            <td><?= h($palavra->vocabulo) ?></td>
        </tr>
        <tr>
            <th><?= __('Idioma') ?></th>
            <td><?= $palavra->has('idioma') ? $this->Html->link($palavra->idioma->id, ['controller' => 'Idiomas', 'action' => 'view', $palavra->idioma->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($palavra->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Active') ?></th>
            <td><?= $this->Number->format($palavra->active) ?></td>
        </tr>
        <tr>
            <th><?= __('Validated') ?></th>
            <td><?= $this->Number->format($palavra->validated) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($palavra->created) ?></tr>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Significado') ?></h4>
        <?= $this->Text->autoParagraph(h($palavra->significado)); ?>
    </div>
</div>

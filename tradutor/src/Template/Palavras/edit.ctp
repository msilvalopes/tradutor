<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $palavra->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $palavra->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Palavras'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Idiomas'), ['controller' => 'Idiomas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Idioma'), ['controller' => 'Idiomas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="palavras form large-9 medium-8 columns content">
    <?= $this->Form->create($palavra) ?>
    <fieldset>
        <legend><?= __('Edit Palavra') ?></legend>
        <?php
            echo $this->Form->input('vocabulo');
            echo $this->Form->input('idioma_id', ['options' => $idiomas]);
            echo $this->Form->input('significado');
            echo $this->Form->input('active');
            echo $this->Form->input('validated');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

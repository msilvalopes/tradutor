<?php

use Phinx\Migration\AbstractMigration;

class AddIdiomas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('idiomas', [
            'collation' => 'utf8_bin'
        ]);

        $table->addColumn('nome', 'string')
              ->addColumn('idioma_id', 'integer')
              ->addColumn('descricao', 'text')
              ->addColumn('created', 'datetime')
              ->addIndex(['idioma_id'])
              ->create();
        $table->save();
        if ($this->hasTable('idiomas')) {
            $this->insertData();
        }
    }
    public function insertData() {
        return;
        $query_insert = 
        'INSERT INTO `idiomas` (`nome`, `idioma_id`, `descricao`, `created`) VALUES (?,?,?,?)';
        $data[] = [
            'nome' => 'Português',
            'idioma_id' => 0,
            'descricao' => 'A língua portuguesa, também designada português, é uma língua românica flexiva '.
                        'originada no galego-português falado no Reino da Galiza e no norte de Portugal. [wiki]',
            'created' => time()
        ];

        foreach ($data as $value) {
            $this->prepare($query_insert)->execute(array_values($value));
        }
    }
}

<?php

use Phinx\Migration\AbstractMigration;

class AddPalavras extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('palavras');
        $table->addColumn('vocabulo', 'string')
            ->addColumn('idioma_id', 'integer')
            ->addColumn('classe_gramatical_id', 'integer')
            ->addColumn('significado', 'text')
            ->addColumn('active', 'integer')
            ->addColumn('validated', 'integer')
            ->addColumn('created', 'datetime')
            ->create();

    }
}

<?php

use Phinx\Migration\AbstractMigration;

class AddUsuarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('usuarios');
        $table->addColumn('login', 'string')
            ->addColumn('password', 'string')
            ->addColumn('nome', 'string')
            ->addColumn('email', 'string')
            ->addColumn('is_admin', 'integer')
            ->addColumn('is_active', 'integer')
            ->addColumn('points', 'integer')
            ->addColumn('created', 'datetime')
            ->create();
        $table = $this->table('login_usuarios');
        $table->addColumn('usuario_id', 'integer')
            ->addColumn('success', 'integer')
            ->addColumn('created', 'datetime')
            ->create();
    }
}
